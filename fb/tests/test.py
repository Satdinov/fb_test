import unittest
from fb.api.services import clean_links


class URLTestCase(unittest.TestCase):
    def test_clean_url(self):
        link_list = [
            'https://url.ru', 'http://url.ru', 'http://url.ru/some_page?getparam=0',
        ]
        clean_links(link_list)
        self.assertEqual(clean_links(link_list), ['url.ru' for _ in range(len(link_list))])

    def test_wrong_url(self):
        not_link_list = [
            'nonlink.', '.notlink', 'n.o.t.l.i.n.k', 'notlink', '123',
        ]
        self.assertEqual(clean_links(not_link_list), [])
