from django.urls import path
from fb.api.views import GetLinkViewSet, PostLinkViewSet


app_name = 'api'

urlpatterns = [
    path('visited_domains/', GetLinkViewSet.as_view(), name='visited_domains'),
    path('visited_links/', PostLinkViewSet.as_view(), name='visited_links'),
]
