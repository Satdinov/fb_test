from urllib.parse import urlparse


def clean_links(links_list):
    clean_list = []
    for i in range(len(links_list)):
        link = links_list[i]
        parsed = urlparse(link).hostname
        if parsed:
            clean_list.append(parsed)
    return clean_list
