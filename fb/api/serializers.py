from rest_framework import serializers


class LinksSerializer(serializers.Serializer):
    links = serializers.CharField()
