import json
import time
import redis

from rest_framework import views
from drf_yasg import openapi
from django.conf import settings
from drf_yasg.utils import swagger_auto_schema
from rest_framework.response import Response
from fb.api.serializers import LinksSerializer
from fb.api.services import clean_links

filter_query_params = [
    openapi.Parameter('from', openapi.IN_QUERY, type=openapi.TYPE_INTEGER, description='От', required=True, ),
    openapi.Parameter('to', openapi.IN_QUERY, type=openapi.TYPE_INTEGER, description='До', required=True, ),
]

redis_instance = redis.StrictRedis(host=settings.REDIS_HOST,
                                   port=settings.REDIS_PORT,
                                   db=0)


class GetLinkViewSet(views.APIView):
    permission_classes = []

    @swagger_auto_schema(
        manual_parameters=filter_query_params,
    )
    def get(self, *args, **kwargs):
        links = set()

        time_from = self.request.query_params.get('from', 0)
        time_to = self.request.query_params.get('to', 0)

        for curr_time in range(int(time_from), int(time_to)):
            if redis_instance.smembers(curr_time):
                links.update(redis_instance.smembers(curr_time))

        items = [link.decode('utf-8') for link in links]

        clean_items = clean_links(items)
        unique_items = set(clean_items)

        count = len(unique_items)

        response = {
            'count': count,
            'msg': f"Found {count} items.",
            'items': unique_items
        }
        return Response(response, status=200)


class PostLinkViewSet(views.APIView):
    permission_classes = []

    @swagger_auto_schema(
        request_body=LinksSerializer,
    )
    def post(self, *args, **kwargs):
        item = json.loads(self.request.body)

        curr_time = round(time.time())

        redis_instance.sadd(curr_time, item['links'])
        response = {
            'msg': f"{curr_time} successfully set to {item['links']}"
        }
        return Response(response, 201)
